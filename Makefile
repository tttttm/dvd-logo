# Makefile

CC=gcc
CFLAGS=-lncurses

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

all: dvd-logo

dvd-logo: dvd-logo.c
	$(CC) -o $@ dvd-logo.c $(CFLAGS)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dvd-logo $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dvd-logo

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dvd-logo
