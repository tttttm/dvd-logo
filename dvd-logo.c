/* dvd-logo.c */

#include <ncurses.h>

#define DELAY         (1)
#define LOGO_W        (58)
#define LOGO_H        (11)
#define INITIAL_X_DIR (2)
#define INITIAL_Y_DIR (-1)
#define EXITCHAR      ('q')

const char *dvd_logo[] = {
  "   .yyyyyyyyyyyyyyyyyyyyo        `syyyyyyyyyyyyyso/-`   \n",
  "   +hhhhhhhdmMMMMMMMNMMMMo      +NMMMNhhhhhhhhdNMMMMMh: \n",
  "  -yyyyy.    `+MMMMMhmMMMM-   :mMMMN+/yyyys     .yMMMMM/\n",
  "  hMMMMd      .MMMMMs.NMMMm`.hMMMN+``NMMMMo      +MMMMM:\n",
  " :MMMMM-   `:sNMMMN+  /MMMMmMMMN+   oMMMMN`   ./yMMMMm: \n",
  " dMMMMMmmNMMMMMds:     sMMMMMm+    `NMMMMNmmNMMMMNdo-   \n",
  ".ooooooooo+/:.          hMMm+      -ooooooooo+/:.       \n",
  "   .-/+osyhddmNNNMMMMMMMMMMMMMMMMMMMMMMNNNmddhyso+/-.   \n",
  "-dNMMMMMMMMMMMMMMMMMMm/-``    ``-/mMMMMMMMMMMMMMMMMMMNd-\n",
  " :+yhmNMMMMMMMMMMMMMMMMmdhyyyyhdmMMMMMMMMMMMMMMMMNmhy+: \n",
  "        `.-:://++oosssssyyyyyyyysssssoo++//::-.`        \n"
};

int screen_h, screen_w;

int x, x_next;
int y, y_next;

int x_direction = INITIAL_X_DIR;
int y_direction = INITIAL_Y_DIR;


int main() {

  /* Initialize screen */
  initscr();
  curs_set(FALSE);
  halfdelay(DELAY);
  noecho();

  /* Get screen size */
  getmaxyx(stdscr, screen_h, screen_w);

  /* Set logo initial position to center of screen */
  y = screen_h/2 - LOGO_H/2;
  x = screen_w/2 - LOGO_W/2;

  while(1) {

    /* Exit program if 'q' is pressed */
    if (getch() == EXITCHAR) break;
    
    clear();

    /* Draw logo */
    for (int i=0; i<LOGO_H; i++) mvprintw(y+i, x, "%s", dvd_logo[i]);

    refresh();

    /* Determine next position of logo */
    y_next = y + y_direction;
    x_next = x + x_direction;

    /* Update collision check vars */
    int tl_corner = y_next < 0                && x_next < 0;
    int tr_corner = y_next < 0                && x_next+LOGO_W >= screen_w;
    int bl_corner = y_next+LOGO_H >= screen_h && x_next < 0;
    int br_corner = y_next+LOGO_H >= screen_h && x_next+LOGO_W >= screen_w;

    /* Reverse direction on corner collision */
    if (tl_corner || tr_corner || bl_corner || br_corner) {
      y_direction *= -1;
      x_direction *= -1;
    }

    /* Reverse direction on left/right collisions */
    else if (x_next < 0 || x_next+LOGO_W-2 > screen_w) {
      x_direction *= -1;
    }

    /* Reverse direction on top/bottom collisions */
    else if (y_next < 0 || y_next+LOGO_H > screen_h) {
      y_direction *= -1;
    }

    /* Increment logo position */
    x += x_direction;
    y += y_direction;

  }

  endwin();

}
