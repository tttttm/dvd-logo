# dvd-logo 

Watch the DVD logo bounce around in your terminal

https://aur.archlinux.org/packages/dvd-logo-git

## Installation

### AUR PKGBUILD
```
git clone https://aur.archlinux.org/dvd-logo-git.git
cd dvd-logo-git
makepkg -si
```

### Yay Package Manager
```
yay -Sy dvd-logo-git
```